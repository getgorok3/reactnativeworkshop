/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ScrollView } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>News</Text>
        </View>

        {/* <View style={styles.content}>     1 content flex row

          <View style={styles.box}>
            <Text>Lorem ...</Text>
          </View>

          <View style={styles.box2}>
            <Text>Lorem ...</Text>
          </View>

        </View> */}

        {/* flex column */}
        <View style={styles.content2}>

          <View style={styles.row}>

            <View style={styles.boxI}>
              <Text style={styles.textInBox}>Lorem ...</Text>
            </View>

            <View style={styles.boxI}>
              <Text style={styles.textInBox}>Lorem ...</Text>
            </View>

          </View>

          <View style={styles.row}>

            <View style={styles.boxI}>
              <Text style={styles.textInBox}>Lorem ...</Text>
            </View>

            <View style={styles.boxI}>
              <Text style={styles.textInBox}>Lorem ...</Text>
            </View>

          </View>

          <View style={styles.row}>

            <View style={styles.boxI}>
              <Text style={styles.textInBox}>Lorem ...</Text>
            </View>

            <View style={styles.boxI}>
              <Text style={styles.textInBox}>Lorem ...</Text>
            </View>

          </View>

        </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,                      // must define space that we want = base
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: '#F5FCFF',
    backgroundColor: 'blue'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  header: {
    alignItems: 'center',  // if inside view must used this
    // backgroundColor: 'red'
  },
  headerText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    padding: 30
  },
  content: {

    backgroundColor: 'yellow',
    flex: 1,
    flexDirection: 'row'    // row this direction >>>  
  },
  box: {
    backgroundColor: 'green',      // if flex 1 each box consume same space if flex 2 and box2 flex 1
    margin: 14,                   // the one will bigger if flex: 0 first just cover text but
    flex: 1                        // second  flex 1 will take over other space
  },
  box2: {
    backgroundColor: 'purple',
    margin: 14,
    flex: 1
  },
  content2: {

    // backgroundColor: 'yellow',
    flex: 1,
    flexDirection: 'column'    // row this direction >>>  
  },
  row: {
    flex: 1,
    flexDirection: 'row'
  },
  boxI: {
    flex: 1, margin: 14, backgroundColor: 'green',
  },
  textInBox: {
    textAlign: 'center', margin: '28%', color: 'white', fontSize: 25
  }


});
// VIEW alway flex = 2direction row and column
// flex: 1 = 100% width if parent not flex: 1 can not used in child
// normal view = auto calculate w and h
//*scrollable = ScrolView ** but must define width and height 
//Text similar to span all text must in tag Text
// zIndex used when we choose position absolute that can move everywhere
// relative = if child is too big it will not over the parent
// fix = not change not move but in mobile fix not stable
//Top to bot same as UI
// the last component will response as the top in view
// zIndex > is more front
// structure ver 1
// tier 1 => main container flex1
// tier 2 => header && footer not set flex but body flex 1
// structure ver 2
// tier 2 = Modal or Drawer and tier 3 = header footer and body