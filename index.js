/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';              // Used for starting component App
import App from './App';                               
import App2 from './App2';  
import App3 from './App3'
import {name as appName} from './app.json';            // everythings about app including name and etc.

AppRegistry.registerComponent(appName, () => App2);
