import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ScrollView, Image, TextInput } from 'react-native';


export default class App3 extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.textICon}>Icon</Text>
                    <Text style={[styles.text, styles.textICon]}>Icon</Text>
                    <Text style={styles.textICon}>Icon</Text>
                </View>
                <View style={[styles.body,styles.center]}>
                    <Text style={[styles.textBody,styles.text]}>ScrollView</Text>
                </View>
                <View style={styles.footer}>
                    <Text style={[styles.text, styles.textICon]}>I</Text>
                    <Text style={ [styles.text, styles.textICon]}>C</Text>
                    <Text style={[styles.text, styles.textICon]}>O</Text>
                    <Text style={[styles.text, styles.textICon]}>N</Text>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
       
        
    },
    header: {
        flexDirection: 'row',

    },
    center: {
     
        alignItems: 'center',
      

    },
    body: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'blue',
        justifyContent: 'center'
    },
    footer: {

       flexDirection: 'row',
       marginHorizontal: -2         // l and r 

    },
    textICon: {
        fontSize: 35,
        color: 'white',
        margin: 2,
        textAlign: 'center',
        backgroundColor: 'green',
    },
    textBody: {
        color: 'white',
        textAlign: 'center',
       
      
    },
    text: {
        flex: 1,
        fontSize: 35,
        
    },
    center: {
        alignItems: 'center',

    }

});