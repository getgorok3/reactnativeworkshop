import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ScrollView, Image, TextInput } from 'react-native';

export default class App2 extends Component {
    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.content}>
                    <View style={styles.row}>
                        <View style={[styles.pictureContainer,styles.center]}>
                            {/* <Image source={{ uri: 'https://www.w3schools.com/w3css/img_lights.jpg' }}
                                style={styles.img}></Image> */}
                                <Text style={[styles.logo]}>Image</Text>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View>
                            <TextInput style={styles.input} value={'TextInput'}></TextInput>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View>
                            <TextInput style={styles.input} value={'TextInput'}></TextInput>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.opacity} >
                            <Text style={{ color: 'white' }}>TouchableOpacity</Text>
                        </View>
                    </View>
                </View>
                {/* <View style={[styles.row, styles.center]}>
                        <View style={[styles.logo, styles.center]}>
                            <Text style={styles.logoText}>Image</Text>
                        </View>
                    
                     <View style={styles.row}>
                         <View style={[styles.input, styles.center]}>
                            <Text style={styles.inputText}Input</Text>
                        </View>
                      </View> 
                      <View style={styles.row}>
                         <View style={[styles.input, styles.center]}>
                            <Text style={styles.inputText}Input</Text>
                        </View>
                      </View> 

                        <View style={[styles.button]}>
                         <View style={[styles.input, styles.center]}>
                            <Text style={styles.inputText}Input</Text>
                        </View>
                      </View> 
                 </View> 
                */}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#666'
    },
    content: {
        flex: 1,
        flexDirection: 'column'
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo:{
      
        textAlign: 'center',
        fontSize: 22,
        color: 'black',
       
        
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
        // flexDirection: 'column',
        // flex: 1,

    },
    img: {
        borderRadius: 150,
        width: '100%', height: '100%'

    },
    pictureContainer: {
        borderRadius: 150,
        width: 200,
        height: 200,
        margin: 20,
        marginBottom: 100,
        backgroundColor: 'white',
        

    },
    input: {
        width: 400,
        textAlign: 'center',
        fontSize: 23,
        borderColor: 'black', borderWidth: 2,
        margin: 10,
        backgroundColor: 'white'
    },
    opacity: {
        width: 400,
        height: 50,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'black',
        marginTop: 80
    }
});